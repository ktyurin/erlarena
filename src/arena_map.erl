%% @doc Map keeps track on creature locations.
%% It has two dimensional list which contains of {X, Y} coordinates mapped on objects located at those coordinates.
%% There are some additional methods to get text representation of the Map which can be used to save it to .txt file.

-module(arena_map).

-behaviour(gen_server).

%% API export
-export([
  add_creature/3,
  create/1,
  move_to/4,
  start_link/1,
  to_string/1
]).

%% Callbacks export
-export([
  code_change/3,
  handle_call/3,
  handle_cast/2,
  handle_info/2,
  init/1,
  terminate/2
]).

-record(state, { % Internal map state
  view % Two dimensional array of {X, Y} points and associated objects, i.e. [
  % {{1, 1}, undefined},
  % {{1, 2}, <0.30.0>},
  % ... ]
  % Object must handle get_view call to be correctly displayed on map
}).

-define(DEFAULT_HEIGHT, 20). % Default map height in tiles
-define(DEFAULT_WIDTH, 20). % Default map width in tiles
-define(NL, "\r\n"). % New line character. TODO: find cross platform solution

%%%%%%%%%
%% API %%
%%%%%%%%%

%% @doc Place creature on map
add_creature(Pid, Creature, Location) ->
  gen_server:cast(Pid, {add_creature, Creature, Location}).

%% @doc Create new map instance. Params value is currently ignored
create(Params) ->
  arena_map_sup:start_child(Params).

%% @doc Move specified creature from one location to another
move_to(Pid, Creature, From, To) ->
  gen_server:call(Pid, {move_to, Creature, From, To}).

%% @doc Function for supervisor to create new map
start_link(Params) ->
  gen_server:start_link(?MODULE, Params, []).

%% @doc Return text representation of map view
to_string(Pid) ->
  gen_server:call(Pid, to_string).

%%%%%%%%%%%%%%%
%% Callbacks %%
%%%%%%%%%%%%%%%

code_change(_OldVsn, State, _Extra) ->
  {ok, State}.

handle_call({move_to, Creature, OldLocation, Location}, _From, State) ->
  NewView = lists:keyreplace(
    Location,
    1,
    lists:keyreplace(OldLocation, 1, State#state.view, {OldLocation, undefined}),
    {Location, Creature}),
  {reply, ok, State#state{view = NewView}};
handle_call(to_string, _From, State) ->
  MapString = create_string(State#state.view),
  {reply, {ok, MapString}, State};
handle_call(_Request, _From, State) ->
  {noreply, State}.

handle_cast({add_creature, Creature, Location}, State) ->
  NewView = lists:keyreplace(Location, 1, State#state.view, {Location, Creature}),
  {noreply, State#state{view = NewView}};
handle_cast(_Request, State) ->
  {noreply, State}.

handle_info(_Info, State) ->
  {noreply, State}.

init(_Args) ->
  View = create_default_map(),
  {ok, #state{view = View}}.

terminate(_Reason, _State) ->
  ok.

%%%%%%%%%%%%%%
%% Internal %%
%%%%%%%%%%%%%%

%% @doc Return view of default height and width map
create_default_map() ->
  create_default_map(?DEFAULT_HEIGHT, ?DEFAULT_WIDTH).

%% @doc Return view of map with specified height and width
create_default_map(Height, Width) ->
% TODO test
  [{{X, Y}, undefined} || X <- lists:seq(0, Width - 1), Y <- lists:seq(0, Height - 1)].

%% @doc Return text representation of the view
create_string(View) ->
  lists:foldl(fun({{_X, Y}, ToDisplay}, String) ->
    ViewChar = get_displayable_char(ToDisplay),
    case Y =:= ?DEFAULT_WIDTH - 1 of
      true ->
        String ++ ViewChar ++ ?NL;
      false ->
        String ++ ViewChar
    end
  end, "", View).

%% @doc Return text representation of the object in the view
get_displayable_char(undefined) ->
  ".";
get_displayable_char(ToDisplay) ->
  {view, ViewChar} = gen_server:call(ToDisplay, get_view),
  ViewChar.