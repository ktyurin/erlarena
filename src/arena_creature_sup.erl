-module(arena_creature_sup).

-behaviour(supervisor).

%% API export
-export([
  start_child/1,
  start_link/0
]).

%% Callbacks export
-export([init/1]).

%%%%%%%%%
%% API %%
%%%%%%%%%

start_child(Params) ->
  supervisor:start_child(?MODULE, [Params]).

start_link() ->
  supervisor:start_link({local, ?MODULE}, ?MODULE, []).

%%%%%%%%%%%%%%%
%% Callbacks %%
%%%%%%%%%%%%%%%

init([]) ->
  Creature = {arena_creature, {arena_creature, start_link, []}, transient, 2000, worker, [arena_creature]},
  Children = [Creature],
  RestartStrategy = {simple_one_for_one, 4, 3600},
  {ok, {RestartStrategy, Children}}.