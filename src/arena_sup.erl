-module(arena_sup).

-behaviour(supervisor).

%% API export
-export([start_link/0]).

%% Callbacks export
-export([init/1]).

%%%%%%%%%
%% API %%
%%%%%%%%%

start_link() ->
  supervisor:start_link({local, ?MODULE}, ?MODULE, []).

%%%%%%%%%%%%%%%
%% Callbacks %%
%%%%%%%%%%%%%%%

init([]) ->
  RoomSup = {arena_room_sup, {arena_room_sup, start_link, []}, permanent, 2000, supervisor, [arena_room_sup]},
  CreatureSup = {arena_creature_sup, {arena_creature_sup, start_link, []}, permanent, 2000, supervisor, [arena_creature_sup]},
  MapSup = {arena_map_sup, {arena_map_sup, start_link, []}, permanent, 2000, supervisor, [arena_map_sup]},
  Children = [RoomSup, CreatureSup, MapSup],
  RestartStrategy = {one_for_one, 4, 3600},
  {ok, {RestartStrategy, Children}}.