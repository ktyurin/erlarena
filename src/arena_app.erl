-module(arena_app).

-behaviour(application).

%% API export
-export([
  start/2,
  stop/1
]).

%%%%%%%%%
%% API %%
%%%%%%%%%

start(_Type, _Args) ->
  case arena_sup:start_link() of
    {ok, Pid} ->
      {ok, Pid};
    Other ->
      {error, Other}
  end.

stop(_State) ->
  ok.