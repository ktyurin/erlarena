%% @doc Room is a container for fighting creatures.
%% There is nothing happens inside the room by default. After 'run' message is received it starts to trigger creatures
%% move every ?MOVE_INTERVAL_MS. On move every creature performs action (move, attack, etc.), Map gets updated and
%% saved to file named room_name.txt. The fight continues until only one creature stays alive in the room.

-module(arena_room).

-behaviour(gen_server).

%% API export
-export([
  add_creature/2,
  create/1,
  run/1,
  start_link/1
]).

%% Callbacks export
-export([
  code_change/3,
  handle_call/3,
  handle_cast/2,
  handle_info/2,
  init/1,
  terminate/2
]).

%% Export for tests
-ifdef(TEST).
-export([
  get_creatures/1,
  perform_move/1,
  save_map/3
]).
-endif.

-record(state, { % Internal room state
  name, % Room name. Log file will have the same name ++ ".txt"
  map, % Reference to room map, process which stores information about creature locations
  moves_performed = 0, % Number of moves performed from start
  creatures = [] % List of creatures currently located in the room
}).

-define(NL, "\r\n"). % New line character. TODO: find cross platform solution
-define(MOVE_INTERVAL_MS, 100). % Interval between moves in ms

%%%%%%%%%
%% API %%
%%%%%%%%%

%% @doc Add creature to the room
add_creature(Pid, Creature) ->
  gen_server:cast(Pid, {add_creature, Creature}).

%% @doc Create new room instance. Params is a list of parameter pairs: [{name, "Room name"}]
create(Params) ->
  arena_room_sup:start_child(Params).

%% @doc Start creatures fight in the room
run(Pid) ->
  gen_server:cast(Pid, run).

%% @doc Function for supervisor to create a new room
start_link(Params) ->
  gen_server:start_link(?MODULE, Params, []).

%%%%%%%%%%%%%%%
%% Callbacks %%
%%%%%%%%%%%%%%%

code_change(_OldVsn, State, _Extra) ->
  {ok, State}.

handle_call(get_creatures, _From, State) ->
  {reply, {ok, State#state.creatures}, State};
handle_call(_Request, _From, State) ->
  {noreply, State}.

handle_cast({add_creature, Creature}, State) ->
  Creatures = State#state.creatures,
  arena_creature:set_map(Creature, State#state.map),
  {noreply, State#state{creatures = [Creature | Creatures]}};
handle_cast(run, State) ->
  file:delete(get_room_filename(State#state.name)),
  schedule_next_move(),
  {noreply, State};
handle_cast(_Request, State) ->
  {noreply, State}.

handle_info(perform_move, State) ->
  perform_move(State),
  case need_next_move(State#state.creatures) of
    true ->
      schedule_next_move(),
      MovesPerformed = State#state.moves_performed,
      {noreply, State#state{moves_performed = MovesPerformed + 1}};
    false ->
      file:write_file(get_room_filename(State#state.name), "Game over!", [append]),
      {stop, normal, State}
  end;
handle_info(_Info, State) ->
  {noreply, State}.

init(Params) ->
  {ok, Map} = arena_map:create([]),
  {ok, #state{
    name = proplists:get_value(name, Params),
    map = Map
  }}.

terminate(_Reason, _State) ->
  ok.

%%%%%%%%%%%%%%
%% Internal %%
%%%%%%%%%%%%%%

%% @doc Return list of creatures currently located in the room
get_creatures(Pid) ->
  gen_server:call(Pid, get_creatures).

%% @doc Return room file name based on room name
get_room_filename(RoomName) ->
  RoomName ++ ".txt".

%% @doc Check if the new move must be scheduled (in case more than one creature stays alive) or the game is over
need_next_move(Creatures) ->
  erlang:length(lists:filter(fun(Creature) -> arena_creature:check_alive(Creature) end, Creatures)) > 1.

%% @doc Make creatures move and save move progress to file
perform_move(State) ->
  lists:foreach(fun(Creature) ->
    arena_creature:perform_move(Creature, State#state.creatures)
  end, State#state.creatures),
  {ok, MapString} = arena_map:to_string(State#state.map),
  save_map(MapString, State#state.name, State#state.moves_performed).

%% @doc Save room map to file
save_map(Map, Name, MoveNumber) ->
  MoveLog = lists:concat(["Move #", MoveNumber, ?NL, Map]),
  file:write_file(get_room_filename(Name), MoveLog, [append]).

%% @doc Start the timer which triggers new move after ?MOVE_INTERVAL_MS period
schedule_next_move() ->
  erlang:send_after(?MOVE_INTERVAL_MS, self(), perform_move).