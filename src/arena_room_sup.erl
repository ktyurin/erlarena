-module(arena_room_sup).

-behaviour(supervisor).

%% API export
-export([
  start_child/1,
  start_link/0
]).

%% Callbacks export
-export([init/1]).

%%%%%%%%%
%% API %%
%%%%%%%%%

start_child(Params) ->
  supervisor:start_child(?MODULE, [Params]).

start_link() ->
  supervisor:start_link({local, ?MODULE}, ?MODULE, []).

%%%%%%%%%%%%%%%
%% Callbacks %%
%%%%%%%%%%%%%%%

init([]) ->
  Room = {arena_room, {arena_room, start_link, []}, transient, 2000, worker, [arena_room]},
  RestartStrategy = {simple_one_for_one, 4, 3600},
  Children = [Room],
  {ok, {RestartStrategy, Children}}.