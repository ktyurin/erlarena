%% @doc Creature behaviour logic.
%% Creature is calm by default. In case of any other creatures are found it tries to move closer within weapon hit range
%% and attack the enemy until its hit points go down to zero.

-module(arena_creature).

-behaviour(gen_server).

%% API export
-export([
  check_alive/1,
  create/1,
  perform_move/2,
  set_map/2,
  start_link/1
]).

%% Callbacks export
-export([
  code_change/3,
  handle_call/3,
  handle_cast/2,
  handle_info/2,
  init/1,
  terminate/2
]).

%% Export for tests
-ifdef(TEST).
-export([
  attack_enemy/2,
  distance_from_to/2,
  filter_self/1,
  find_closest/2,
  find_closest_among/1,
  find_enemy/2,
  get_location/1,
  get_race_view/1,
  get_view/2,
  hit/2,
  is_alive/1,
  is_reachable_from/3,
  map_creatures_to_distances/2,
  move_to/3,
  step_closer/2
]).
-endif.

-record(weapon, { % Internal weapon state
  type, % Type, i.e. sword|axe|pole|etc. Currently not used
  range, % Range in tiles
  damage % Number of damage points given by the weapon
}).

-record(state, { % Internal creature state
  race, % Race. Can be human|orc|elf. Used for text creature representation on the map
  str, % Strength increases creature damage
  dex, % Dexterity can help to avoid hits. Currenty not used
  hp, % Hit points. Number of damage creature can take before die
  enemy, % Current enemy. Creature will try to move to the enemy and hit it with the weapon
  location, % Current position on the map as {X, Y} tuple
  map, % Reference to current map
  weapon, % Current weapon represented by record 'weapon'
  name % Readable name. Currently not used
}).

%%%%%%%%%
%% API %%
%%%%%%%%%

%% @doc Check if creature has positive hit points and can act
check_alive(Pid) ->
  gen_server:call(Pid, check_alive).

%% @doc Create new creature instance. Params is a list of parameter pairs: [{race, human}, {str, 10}, {hp, 50}, ...]
create(Params) ->
  arena_creature_sup:start_child(Params).

%% @doc Make creature act. Look for enemy, move, attack, etc.
perform_move(Pid, Creatures) ->
  gen_server:call(Pid, {perform_move, Creatures}).

%% @doc Move creature to another map
set_map(Pid, Map) ->
  gen_server:cast(Pid, {set_map, Map}).

%% @doc Function for supervisor to create new creature
start_link(Params) ->
  gen_server:start_link(?MODULE, Params, []).

%%%%%%%%%%%%%%%
%% Callbacks %%
%%%%%%%%%%%%%%%

code_change(_OldVsn, State, _Extra) ->
  {ok, State}.

handle_call(check_alive, _From, State) ->
  Result = is_alive(State#state.hp),
  {reply, Result, State};
handle_call(get_location, _From, State) ->
  {reply, {location, State#state.location}, State};
handle_call(get_view, _From, State) ->
  View = get_view(State#state.race, State#state.hp),
  {reply, {view, View}, State};
handle_call({hit, Damage}, _From, State) ->
  HpLeft = State#state.hp - Damage,
  Result = is_alive(HpLeft),
  {reply, Result, State#state{hp = HpLeft}};
handle_call({perform_move, Creatures}, _From, State) ->
  case is_alive(State#state.hp) of
    true ->
      NewState = handle_perform_move(Creatures, State),
      {reply, ok, NewState};
    false ->
      {reply, ok, State}
  end;
handle_call(_Request, _From, State) ->
  {noreply, State}.

handle_cast({set_map, Map}, State) ->
  arena_map:add_creature(Map, self(), State#state.location),
  {noreply, State#state{map = Map}};
handle_cast(_Request, State) ->
  {noreply, State}.

handle_info(_Info, State) ->
  {noreply, State}.

init(Params) ->
  {ok, #state{
    race = proplists:get_value(race, Params),
    str = proplists:get_value(str, Params),
    dex = proplists:get_value(dex, Params),
    hp = proplists:get_value(hp, Params),
    location = proplists:get_value(location, Params),
    weapon = #weapon{
      type = proplists:get_value(weapon_type, Params),
      damage = proplists:get_value(weapon_damage, Params),
      range = proplists:get_value(weapon_range, Params)
    },
    name = proplists:get_value(name, Params)
  }}.

terminate(_Reason, _State) ->
  ok.

%%%%%%%%%%%%%%
%% Internal %%
%%%%%%%%%%%%%%

%% @doc Make attempt to hit specified creature
attack_enemy(Enemy, {Str, _Dex, Damage}) ->
  hit(Enemy, Damage + Str).

%% @doc Calculate distance to target
distance_from_to({MyX, MyY}, {HisX, HisY}) ->
  math:sqrt(math:pow(MyX - HisX, 2) + math:pow(MyY - HisY, 2));
distance_from_to({X, Y}, Creature) ->
  {location, {CreatureX, CreatureY}} = get_location(Creature),
  distance_from_to({X, Y}, {CreatureX, CreatureY}).

%% @doc Filter list to exclude self() reference
filter_self(Creatures) ->
  lists:filter(fun(Creature) -> Creature =/= self() end, Creatures).

%% @doc Choose the closest between two specified creatures
find_closest({FirstDistance, FirstCreature}, {SecondDistance, SecondCreature}) ->
  case FirstDistance < SecondDistance of
    true ->
      {FirstDistance, FirstCreature};
    false ->
      {SecondDistance, SecondCreature}
  end.

%% @doc Choose the closest between list of specified creatures
find_closest_among(CreaturesToDistances) ->
  {_Distance, Closest} = lists:foldl(fun find_closest/2, {not_a_number, undefined}, CreaturesToDistances),
  Closest.

%% @doc Find enemy on the map
find_enemy(Creatures, _Location) when Creatures =:= []; Creatures =:= [self()] ->
  not_found;
find_enemy(Creatures, {X, Y}) ->
  CreaturesWithoutSelf = filter_self(Creatures),
  CreaturesToDistances = map_creatures_to_distances({X, Y}, CreaturesWithoutSelf),
  Enemy = find_closest_among(CreaturesToDistances),
  {found, Enemy}.

%% @doc Return any creature location on the map
get_location(Pid) ->
  gen_server:call(Pid, get_location).

%% @doc Return creature text representation based on its race
get_race_view(Race) ->
  case Race of
    human ->
      "H";
    orc ->
      "O";
    elf ->
      "E";
    _ ->
      "?"
  end.

%% @doc Return creature text representation based on its race and hit points
get_view(Race, Hp) ->
  case is_alive(Hp) of
    true ->
      get_race_view(Race);
    false ->
      "%"
  end.

%% @doc Perform move. Look for enemy, move closer, attack
handle_perform_move(Creatures, State = #state{enemy = undefined}) ->
  case find_enemy(Creatures, State#state.location) of
    not_found ->
      State;
    {found, Enemy} ->
      handle_perform_move(Creatures, State#state{enemy = Enemy})
  end;
handle_perform_move(_Creatures, State = #state{enemy = Enemy}) ->
  case is_reachable_from(
    Enemy,
    State#state.location,
    State#state.weapon#weapon.range
  ) of
    true ->
      attack_enemy(Enemy, {State#state.str, State#state.dex, State#state.weapon#weapon.damage}),
      State;
    false ->
      {NewX, NewY} = move_to(Enemy, State#state.location, State#state.map),
      State#state{location = {NewX, NewY}}
  end.

%% @doc Send hit notification to creature
hit(Pid, Damage) ->
  gen_server:call(Pid, {hit, Damage}).

%% @doc Check if creature has hit points and alive
is_alive(Hp) ->
  Hp > 0.

%% @doc Check if specified target is reachable from specified position
is_reachable_from(Creature, {X, Y}, Range) ->
  {location, {CreatureX, CreatureY}} = get_location(Creature),
  Distance = distance_from_to({X, Y}, {CreatureX, CreatureY}),
  FloorDistance = arena_math:floor(Distance),
  Range >= FloorDistance.

%% @doc Return list of creatures mapped to distance from specified location
map_creatures_to_distances({X, Y}, Creatures) ->
  lists:map(fun(Creature) ->
    Distance = distance_from_to({X, Y}, Creature),
    {Distance, Creature}
  end, Creatures).

%% @doc Perform move attempt to specified creature
move_to(Creature, {X, Y}, Map) ->
  {location, {CreatureX, CreatureY}} = get_location(Creature),
  {NewX, NewY} = step_closer({X, Y}, {CreatureX, CreatureY}),
  case arena_map:move_to(Map, self(), {X, Y}, {NewX, NewY}) of
    ok ->
      {NewX, NewY};
    prohibited ->
      {X, Y}
  end.

%% @doc Check if it's possible to come closer to specified location and return new location if it is
step_closer({FromX, FromY}, {ToX, ToY}) ->
  NewX = step_closer(FromX, ToX),
  NewY = step_closer(FromY, ToY),
  {NewX, NewY};
step_closer(From, To) ->
  case erlang:abs(From - To) > 1 of
    true when From > To ->
      From - 1;
    true when From < To ->
      From + 1;
    false ->
      From
  end.