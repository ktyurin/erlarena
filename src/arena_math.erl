%% @doc Helper module for math operations.

-module(arena_math).

%% API export
-export([floor/1]).

%%%%%%%%%
%% API %%
%%%%%%%%%

floor(X) ->
  T = erlang:trunc(X),
  case (X - T) of
    Neg when Neg < 0 -> T - 1;
    Pos when Pos > 0 -> T;
    _ -> T
  end.