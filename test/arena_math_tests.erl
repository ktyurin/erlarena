-module(arena_math_tests).

-include_lib("eunit/include/eunit.hrl").

arena_math_test_() ->
  [
    {"Function must calculate number floor", fun check_floor/0}
  ].

check_floor() ->
  ?assertMatch(3, arena_math:floor(3.9)).