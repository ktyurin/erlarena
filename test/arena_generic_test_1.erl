-module(arena_generic_test_1).

-include_lib("eunit/include/eunit.hrl").

arena_generic_test() ->
  application:start(arena),
  {ok, Room} = arena_room:create([{name, "room"}]),
  {ok, Human} = arena_creature:create([
    {race, human},
    {str, 5},
    {dex, 5},
    {hp, 50},
    {location, {0, 0}},
    {weapon_type, sword},
    {weapon_damage, 5},
    {weapon_range, 1},
    {name, "Lancelot"}
  ]),
  {ok, Orc} = arena_creature:create([
    {race, orc},
    {str, 10},
    {dex, 1},
    {hp, 100},
    {location, {19, 19}},
    {weapon_type, axe},
    {weapon_damage, 10},
    {weapon_range, 1},
    {name, "Grr'Hrr"}
  ]),
  arena_room:add_creature(Room, Human),
  arena_room:add_creature(Room, Orc),
  arena_room:run(Room),
  timer:sleep(2000),
  application:stop(arena).