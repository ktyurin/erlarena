-module(arena_creature_tests).

-include_lib("eunit/include/eunit.hrl").

arena_creature_test_() ->
  {foreach,
    fun startup/0,
    fun cleanup/1,
    [
      {"Function must return arena_creature pid on creation.", fun check_create/0},
      {
        "Function must calculate distance between two {X, Y} points on 2D map.",
        fun check_distance_from_point_to_point/0
      },
      {
        "Function must calculate distance between {X, Y} point and creature coordinates.",
        fun check_distance_from_point_to_creature/0
      },
      {
        "Function must find the closest creature in list and mustn't find anyone in case list is empty or " ++
          "consists of self() only.",
        fun check_find_enemy/0
      },
      {"Function must return list with self() filtered out.", fun check_filter_self/0},
      {
        "Function must return mapping of distances between creatures and specified point to creatures",
        fun check_map_creatures_to_distances/0
      },
      {"Function must choose between two creatures with least distance", fun check_find_closest/0},
      {"Function must choose from list the creature with least distance", fun check_find_closest_among/0},
      {
        "Function must check if creature is reachable from specified location by specified range",
        fun check_is_reachable_from/0
      },
      {"Function must return current creature location", fun check_get_location/0},
      {"Function must return 'dead' if damage was above creature HP and alive otherwise", fun check_hit/0},
      {"Function must deliver damage to creature equals to passed Strength plus Damage", fun check_attack_enemy/0},
      {"Function must return new location after moving to creature", fun check_move_to/0},
      {"Function must return new coordinate after moving to creature", fun check_step_closer/0},
      {"Function must return correct representation of the creature on the map", fun check_get_race_view/0},
      {"Function must return correct representation of the creature on the map, dead or alive", fun check_get_view/0},
      {"Function must check if the creature is alive by his state HP", fun check_check_alive/0}
    ]
  }.

startup() ->
  application:start(arena).

cleanup(_) ->
  application:stop(arena).

create() ->
  arena_creature:create([]).

check_create() ->
  ?assertMatch({ok, _Creature}, create()).

check_distance_from_point_to_point() ->
  ?assertMatch(5.0, arena_creature:distance_from_to({5, 6}, {1, 3})).

check_distance_from_point_to_creature() ->
  {ok, Creature} = arena_creature:create([{location, {5, 6}}]),
  ?assertMatch(5.0, arena_creature:distance_from_to({1, 3}, Creature)).

check_find_enemy() ->
  ?assertMatch(not_found, arena_creature:find_enemy([], undefined)),
  ?assertMatch(not_found, arena_creature:find_enemy([self()], undefined)),
  {ok, Creature1} = arena_creature:create([{location, {1, 1}}]),
  {ok, Creature2} = arena_creature:create([{location, {3, 3}}]),
  {ok, Creature3} = arena_creature:create([{location, {7, 7}}]),
  {ok, Creature4} = arena_creature:create([{location, {9, 9}}]),
  ?assertMatch({found, Creature2}, arena_creature:find_enemy([
    Creature1,
    Creature2,
    Creature3,
    Creature4
  ], {4, 4})).

check_filter_self() ->
  {ok, Creature1} = create(),
  {ok, Creature2} = create(),
  ?assertMatch([Creature1, Creature2], arena_creature:filter_self([Creature1, self(), Creature2])).

check_map_creatures_to_distances() ->
  {ok, Creature1} = arena_creature:create([{location, {1, 3}}]),
  {ok, Creature2} = arena_creature:create([{location, {3, 3}}]),
  ?assertMatch(
    [{5.0, Creature1}, {3.605551275463989, Creature2}],
    arena_creature:map_creatures_to_distances({5, 6}, [Creature1, Creature2])
  ).

check_find_closest() ->
  {ok, Creature1} = create(),
  {ok, Creature2} = create(),
  ?assertMatch({3, Creature1}, arena_creature:find_closest({3, Creature1}, {5, Creature2})).

check_find_closest_among() ->
  {ok, Creature1} = create(),
  {ok, Creature2} = create(),
  {ok, Creature3} = create(),
  ?assertMatch(Creature2, arena_creature:find_closest_among([{8, Creature1}, {2, Creature2}, {6, Creature3}])).

check_is_reachable_from() ->
  {ok, Creature} = arena_creature:create([{location, {1, 3}}]),
  ?assertMatch(true, arena_creature:is_reachable_from(Creature, {1, 4}, 1)),
  ?assertMatch(true, arena_creature:is_reachable_from(Creature, {2, 4}, 1)),
  ?assertMatch(false, arena_creature:is_reachable_from(Creature, {4, 5}, 2)).

check_get_location() ->
  {ok, Creature} = arena_creature:create([{location, {1, 3}}]),
  ?assertMatch({location, {1, 3}}, arena_creature:get_location(Creature)).

check_hit() ->
  {ok, Creature} = arena_creature:create([{hp, 10}]),
  ?assertMatch(true, arena_creature:hit(Creature, 3)),
  ?assertMatch(false, arena_creature:hit(Creature, 7)).

check_attack_enemy() ->
  {ok, Creature} = arena_creature:create([{hp, 10}]),
  ?assertMatch(true, arena_creature:attack_enemy(Creature, {2, 3, 1})),
  ?assertMatch(false, arena_creature:attack_enemy(Creature, {4, 3, 3})).

check_move_to() ->
  {ok, Map} = arena_map:create([]),
  {ok, Creature1} = arena_creature:create([{location, {5, 3}}]),
  {ok, Creature2} = arena_creature:create([{location, {5, 3}}]),
  {ok, Creature3} = arena_creature:create([{location, {5, 3}}]),
  ?assertMatch({8, 5}, arena_creature:move_to(Creature1, {9, 6}, Map)),
  ?assertMatch({6, 4}, arena_creature:move_to(Creature2, {6, 4}, Map)),
  ?assertMatch({5, 3}, arena_creature:move_to(Creature3, {5, 3}, Map)).

check_step_closer() ->
  ?assertMatch(3, arena_creature:step_closer(2, 4)),
  ?assertMatch(3, arena_creature:step_closer(3, 4)),
  ?assertMatch(4, arena_creature:step_closer(4, 4)),
  ?assertMatch(5, arena_creature:step_closer(5, 4)),
  ?assertMatch(5, arena_creature:step_closer(6, 4)).

check_get_race_view() ->
  ?assertMatch("H", arena_creature:get_race_view(human)),
  ?assertMatch("O", arena_creature:get_race_view(orc)),
  ?assertMatch("E", arena_creature:get_race_view(elf)),
  ?assertMatch("?", arena_creature:get_race_view(mickey_mouse)).

check_get_view() ->
  ?assertMatch("H", arena_creature:get_view(human, 1)),
  ?assertMatch("%", arena_creature:get_view(human, 0)),
  ?assertMatch("%", arena_creature:get_view(human, -1)).

check_check_alive() ->
  {ok, Creature1} = arena_creature:create([{hp, 1}]),
  {ok, Creature2} = arena_creature:create([{hp, 0}]),
  {ok, Creature3} = arena_creature:create([{hp, -1}]),
  ?assertMatch(true, arena_creature:check_alive(Creature1)),
  ?assertMatch(false, arena_creature:check_alive(Creature2)),
  ?assertMatch(false, arena_creature:check_alive(Creature3)).