-module(arena_room_tests).

-include_lib("eunit/include/eunit.hrl").

arena_room_test_() ->
  {foreach,
    fun startup/0,
    fun cleanup/1,
    [
      {"Function must return room pid on creation", fun check_create/0},
      {"Function must add creature to room", fun check_add_creature/0}
    ]
  }.

startup() ->
  application:start(arena).

cleanup(_) ->
  application:stop(arena).

create() ->
  arena_room:create([{name, "default"}]).

check_create() ->
  ?assertMatch({ok, _Pid}, create()).

create_creature() ->
  arena_creature:create([]).

check_add_creature() ->
  {ok, Room} = create(),
  {ok, Creature} = create_creature(),
  ?assertMatch(ok, arena_room:add_creature(Room, Creature)),
  ?assertMatch({ok, [Creature]}, arena_room:get_creatures(Room)).