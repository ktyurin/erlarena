-module(arena_map_tests).

-include_lib("eunit/include/eunit.hrl").

arena_room_test_() ->
  {foreach,
    fun startup/0,
    fun cleanup/1,
    [
      {"Function must return map pid on creation", fun check_create/0}
    ]
  }.

startup() ->
  application:start(arena).

cleanup(_) ->
  application:stop(arena).

create() ->
  arena_map:create([]).

check_create() ->
  ?assertMatch({ok, _Pid}, create()).